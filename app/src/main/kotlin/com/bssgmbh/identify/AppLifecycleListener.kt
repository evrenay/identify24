package com.bssgmbh.identify

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.LifecycleObserver
import com.evren.repository.model.enum.NavGraph
import com.evren.repository.model.enum.SocketConnectionStatus
import com.evren.repository.shared.SharedValues
import com.evren.repository.socket.SocketSource


class AppLifecycleListener constructor(val socketSource: SocketSource,val sharedValues: SharedValues) : LifecycleObserver {


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {


    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
        socketSource.webSocket?.let {
        sharedValues.changeNavGraph.value = NavGraph.FORM.name
        sharedValues.socketStatus.value = SocketConnectionStatus.UNIDENTIFIED.type
                it.close(4001,"")

        }
    }


}