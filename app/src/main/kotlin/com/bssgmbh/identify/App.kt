package com.bssgmbh.identify

import androidx.lifecycle.ProcessLifecycleOwner
import com.bssgmbh.identify.di.DaggerAppComponent
import com.evren.core.di.DaggerCoreComponent
import com.evren.repository.shared.SharedValues
import com.evren.repository.socket.SocketSource
import com.onesignal.OSNotification
import com.onesignal.OneSignal
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import org.bouncycastle.jce.provider.BouncyCastleProvider
import timber.log.Timber
import java.security.Security
import javax.inject.Inject


class App : DaggerApplication() {

    @Inject
    internal lateinit var socketSource: SocketSource

    @Inject
    internal lateinit var sharedValues: SharedValues

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.factory()
            .create(
                DaggerCoreComponent.factory()
                    .create(this)
            )


    override fun onCreate() {
        super.onCreate()
        Security.insertProviderAt(BouncyCastleProvider(), 1)
        val applistener =
            AppLifecycleListener(socketSource, sharedValues)
        ProcessLifecycleOwner.get().lifecycle.addObserver(applistener)

        Timber.plant(Timber.DebugTree())

        OneSignal.startInit(this)
            .setNotificationReceivedHandler(OneSignalNotificationReceivedHandler())
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }

    internal inner class OneSignalNotificationReceivedHandler : OneSignal.NotificationReceivedHandler {
        override fun notificationReceived(notification: OSNotification) {
            val data = notification.payload.additionalData
            val customKey: String?
            OSNotification.DisplayType.None
            if (data != null) {
                customKey = data.optString("customkey", null!!)
            }
        }
    }
}
