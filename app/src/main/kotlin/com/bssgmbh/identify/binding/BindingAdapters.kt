package com.bssgmbh.identify.binding

import android.widget.EditText
import androidx.core.widget.NestedScrollView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData

@BindingAdapter("error","scrollView")
fun EditText.error(errorStr : MutableLiveData<String>, scrollView : NestedScrollView?) {
    errorStr.value?.let {
        this.error = it
        scrollView?.post {
            scrollView.scrollTo(0, this.top)
            this.requestFocus()
        }

    }
}