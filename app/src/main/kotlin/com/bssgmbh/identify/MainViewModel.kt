package com.bssgmbh.identify

import com.evren.core.base.viewmodel.BaseViewModel
import com.evren.repository.interactors.customer.GetCustomerInfoFromDb
import com.evren.repository.interactors.base.None
import com.evren.repository.interactors.base.handle
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.shared.SharedValues
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@ExperimentalCoroutinesApi
class MainViewModel @Inject constructor(
    val getCustomerInfoFromDb : GetCustomerInfoFromDb,
    val sharedValues: SharedValues
) : BaseViewModel<CustomerInformationEntity?>() {



    override suspend fun loadData() {
        getCustomerInfoFromDb(None()).collect {
            it.handle(::handleState,::handleFailure,::handleSuccess)
        }
    }




}