package com.bssgmbh.identify

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import androidx.activity.viewModels
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.bssgmbh.mrz.ui.NfcFragment
import com.evren.common.dialog.NoInternetDialogFragment
import com.evren.common.dialog.alert
import com.evren.common.dialog.positiveButton
import com.evren.core.broadcast_service.NetworkStateReceiver
import com.evren.core.di.ViewModelFactory
import com.evren.repository.model.SocketActionType
import com.evren.repository.model.enum.NavGraph
import com.evren.repository.model.enum.SocketConnectionStatus
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import javax.inject.Inject


@ExperimentalCoroutinesApi
class MainActivity : DaggerAppCompatActivity(), NetworkStateReceiver.NetworkStateReceiverListener,NoInternetDialogFragment.NoInternetClickInterface {


    var noInternetAlertDialogFragment : NoInternetDialogFragment?= null


    private lateinit var navController: NavController

    private var networkStateReceiver : NetworkStateReceiver?= null

    private var adapter : NfcAdapter ?= null


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by viewModels<MainViewModel> { viewModelFactory }



    private val addOnDestinationChangedListener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
            when(destination.id){
                R.id.nfcFragment->{
                    startNfcReader()
                }
                else->{
                    clearNfcReader()
                }
            }
        }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLocaleSharedPreferances()?.let { setLocale(it,false) }
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) // close screen saver
        navController = findNavController(R.id.nav_host_fragment)
        navController.addOnDestinationChangedListener(addOnDestinationChangedListener)

         observeDataChanges()



      }


    private fun startNfcReader(){
        adapter = NfcAdapter.getDefaultAdapter(this)
        val intent = Intent(this, this.javaClass)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent =
            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val filter =
            arrayOf(arrayOf("android.nfc.tech.IsoDep"))
        adapter?.enableForegroundDispatch(this, pendingIntent, null, filter)
        checkNfcEnabled()
    }

    private fun checkNfcEnabled() {
        if (adapter?.isEnabled != true){
            showNfcDialog()
            return
        }
    }

    private fun showNfcDialog(){
        alert {
            setTitle(getString(R.string.nfc_off) )
            setMessage(getString(R.string.nfc_off_desc))
            positiveButton(text = getString(R.string.go_to_setting)) {
                startActivity(Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)) }
            }

    }



    fun clearNfcReader(){
        adapter?.let {
            it.disableForegroundDispatch(this)
            adapter = null
        }
    }





    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (NfcAdapter.ACTION_TECH_DISCOVERED == intent?.action) {
            val tag =
                intent.extras?.getParcelable<Tag>(NfcAdapter.EXTRA_TAG)
            tag?.techList?.let {
                if (it.contains("android.nfc.tech.IsoDep")) {
                    if (navController.currentDestination?.id  == R.id.nfcFragment){
                        val nfcFragment = supportFragmentManager.fragments.first().childFragmentManager.fragments.first()  as  NfcFragment
                        val isoDep = IsoDep.get(tag)
                        isoDep.timeout = 5000
                        nfcFragment.triggerNfcReader(isoDep)
                        nfcFragment.finishedNfcProcess = {
                            clearNfcReader()
                            startNfcReader()
                        }
                    }
                }}}

    }

    override fun onResume() {
        super.onResume()
        if (navController.currentDestination?.id  == R.id.nfcFragment){
            startNfcReader()
        }
    }


    override fun onPause() {
        super.onPause()
        clearNfcReader()
    }



    private fun observeDataChanges() {
        viewModel.sharedValues.changeNavGraph.observe(this){
            it?.let {
                viewModel.sharedValues.changeNavGraph.value = null
                when (it){
                    NavGraph.FORM.name->{
                        openDestination(R.id.nav_form)
                    }
                    NavGraph.CALL.name->{
                        openDestination(R.id.nav_call)
                    }
                    NavGraph.MRZ.name->{
                        openDestination(R.id.nav_mrz)
                    }

                }
            }
        }

        viewModel.sharedValues.changeLanguage.observe(this){
            it?.let { lang ->
                viewModel.sharedValues.changeLanguage.value = null
                setLocale(lang,true)
            }

        }

        viewModel.successData.observe(this){
            //openDestination(R.id.nav_form)
            closeSplash()
        }

        viewModel.errorData.observe(this){
            closeSplash()
        }


        viewModel.sharedValues.inComingCall.observe(this){
            it?.let { socketResponse ->
                if (socketResponse.action == SocketActionType.INIT_CALL.type){
                    supportFragmentManager.fragments.forEach { fragment->
                        if (fragment.isVisible){
                            socketResponse.room?.let { room->
                                openDestination(R.navigation.nav_call)
                                return@forEach
                            }
                        }
                    }
                }
            }
        }
    }


    private fun setLocale(selectedLocale: String, restart: Boolean) {
        var locale : Locale ?= null
        if (selectedLocale == NO_SELECTED_LANGUAGE){
            val defLanguage = Locale.getDefault().language
            if (defLanguage == "en" || defLanguage == "tr" || defLanguage == "de"){
                 locale = Locale(defLanguage)
            }else {
                 locale = Locale("en")
            }

        }else{
            locale = Locale(selectedLocale)
        }
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )
        if (restart){
            writeLocaleSharedPreferances(selectedLocale)
            finish()
            val i = Intent(this, MainActivity::class.java)
            startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            overridePendingTransition(0, 0);


        }


    }

    fun writeLocaleSharedPreferances(selectedLocale: String) {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(SELECTED_LANGUAGE,selectedLocale)
        editor.apply()
    }


    fun getLocaleSharedPreferances(): String? {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        return sharedPref.getString(SELECTED_LANGUAGE, NO_SELECTED_LANGUAGE)
    }


    private fun closeSplash(){
        findViewById<RelativeLayout>(R.id.relLaySplash).visibility = View.GONE
    }


     private fun openDestination(destination : Int){
        val navGraph = navController.graph
        navGraph.startDestination = destination
        navController.graph = navGraph
        closeSplash()
    }


   override fun networkAvailable() {
        clearNetworkDialog()
    }
    private fun clearNetworkDialog(){
        noInternetAlertDialogFragment?.let {
            it.dismissAllowingStateLoss()
            noInternetAlertDialogFragment = null
        }
    }


    override fun networkUnavailable() {
        if (noInternetAlertDialogFragment == null){
            viewModel.sharedValues.socketStatus.value = SocketConnectionStatus.CLOSE.type
            noInternetAlertDialogFragment = NoInternetDialogFragment.newInstance()
            supportFragmentManager.beginTransaction().add(noInternetAlertDialogFragment!!,NoInternetDialogFragment::class.java.toString()).commitAllowingStateLoss()
            noInternetAlertDialogFragment?.isCancelable = true
        }

    }



     override fun noInternetClickListener() {
        clearNetworkDialog()
    }

companion object{
    val SELECTED_LANGUAGE = "selectedLocale"
    val NO_SELECTED_LANGUAGE = "no_language"
}

}
