package com.bssgmbh.identify.di

import androidx.lifecycle.ViewModel
import com.evren.common.di.CommonContributor
import com.evren.core.di.keys.ViewModelKey
import com.evren.form.di.FormContributor
import com.bssgmbh.identify.MainActivity
import com.bssgmbh.identify.MainViewModel
import com.bssgmbh.mrz.di.MrzContributor
import com.evren.webrtc.di.CallContributor
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class AppModule {


    @ContributesAndroidInjector(
        modules = [
            FormContributor::class,
            CallContributor::class,
            CommonContributor::class,
            MrzContributor::class]
    )
    abstract fun mainActivity(): MainActivity





    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun mainViewModel(mainViewModel: MainViewModel): ViewModel


}
