package com.bssgmbh.identify.di

import javax.inject.Scope

@Scope
annotation class AppScope
