package com.evren.common.base

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import com.evren.core.base.lifecycle.BaseDaggerFragment


abstract class IdentifyBaseFragment<T : ViewDataBinding> : BaseDaggerFragment<T>() {

    var isFragmentFirstCreate = true






    abstract fun observeDataChanges()






    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(isFragmentFirstCreate){
            observeDataChanges()
            isFragmentFirstCreate = false
        }


    }







}