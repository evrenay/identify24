package com.evren.common.di

import com.evren.common.di.modules.CommonModule
import com.evren.common.di.scopes.CommonScope
import com.evren.common.dialog.AreYouSureDialogFragment
import com.evren.common.dialog.NoInternetDialogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CommonContributor {


    @ContributesAndroidInjector(
        modules = [CommonModule::class]
    )
    @CommonScope
    abstract fun noInternetDialogFragment(): NoInternetDialogFragment

    @ContributesAndroidInjector(
        modules = [CommonModule::class]
    )
    @CommonScope
    abstract fun areYouSureDialogFragment(): AreYouSureDialogFragment

}