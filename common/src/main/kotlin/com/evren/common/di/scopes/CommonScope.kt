package com.evren.common.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class CommonScope