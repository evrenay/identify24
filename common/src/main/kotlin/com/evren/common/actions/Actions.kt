package com.evren.common.actions

import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.evren.common.R


/**
 * Navigation actions for navigation between feature activities
 */

fun Fragment.openCall() =
    NavHostFragment.findNavController(this).navigate(Uri.parse(getString(R.string.call_deeplink)))

fun Fragment.openMrz(identId : String) =
    NavHostFragment.findNavController(this).navigate(Uri.parse(getString(R.string.mrz_uri,identId)))

fun Fragment.openForm() =
    NavHostFragment.findNavController(this).navigate(Uri.parse(getString(R.string.form_deeplink_and_uri)))




