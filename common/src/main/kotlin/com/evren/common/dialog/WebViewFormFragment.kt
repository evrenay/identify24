package com.evren.common.dialog

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.DialogFragment
import com.evren.common.R
import com.evren.common.base.IdentifyBaseDialogFragment
import com.evren.common.databinding.FragmentWebviewFormBinding

class WebViewFormFragment : IdentifyBaseDialogFragment<FragmentWebviewFormBinding>() {

    //region Properties

    var listener: WebViewFormListener? = null

    //endregion

    //region Functions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)

    }


    override fun onResume() {
        super.onResume()
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog?.window?.setLayout(width, height)
        }
        dialog?.setOnKeyListener { dialog, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (event.action == KeyEvent.ACTION_DOWN){
                    listener?.onFailure()
                    true
                }
                else{
                    true
                }


            } else
                false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.isNestedScrollingEnabled = true
        binding.viewTitle.ivBack.setOnClickListener {
            listener?.onFailure()
        }
        binding.webView.webChromeClient = object : WebChromeClient() {

            override fun onProgressChanged(view: WebView?, progress: Int) {
                if (progress < 100) {
                    if (this@WebViewFormFragment.isAdded) binding.loading.relLayLoading.visibility = View.VISIBLE
                }

                if (progress == 100) {
                    if (this@WebViewFormFragment.isAdded) binding.loading.relLayLoading.visibility = View.GONE
                }
            }

            override fun onCloseWindow(window: WebView?) {
                listener?.onFailure()
            }

        }

        binding.webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                url: String?
            ): Boolean {
                return false
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                val uri: Uri = Uri.parse(url)
                val isSuccess: String? = uri.getQueryParameter("success")

                if (isSuccess == "true" ) {
                    listener?.onSuccess()
                }
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                listener?.onFailure()
            }


        }

        arguments?.getString("webViewUrl")?.let {
            binding.webView.loadUrl(it)
            binding.webView.requestFocus()
        }




    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (parentFragment is WebViewFormListener) {
            listener = parentFragment as WebViewFormListener
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }


    override fun onDestroyView() {
        binding.webView.stopLoading()
        super.onDestroyView()
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {

        @JvmStatic
        fun newInstance(webViewUrl: String) =
            WebViewFormFragment().apply {
                arguments = Bundle().apply {
                  putString("webViewUrl", webViewUrl)
                }
            }
    }

    override fun observeDataChanges() {
    }



    override fun getLayoutId(): Int = R.layout.fragment_webview_form

    //endregion

}