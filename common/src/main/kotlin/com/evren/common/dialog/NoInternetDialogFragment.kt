package com.evren.common.dialog

import android.content.Context
import android.os.Bundle
import android.view.View
import com.evren.common.base.IdentifyBaseDialogFragment
import javax.inject.Inject
import com.evren.common.R
import com.evren.common.databinding.NoInternetDialogBinding


class NoInternetDialogFragment @Inject constructor() : IdentifyBaseDialogFragment<NoInternetDialogBinding>() {


    private var exit = false

    var listener : NoInternetClickInterface ?=null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tryAgainBtn.setOnClickListener {
            listener?.noInternetClickListener()
        }
    }


    interface NoInternetClickInterface{
        fun noInternetClickListener()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (activity is NoInternetClickInterface) {
            listener = activity as NoInternetClickInterface
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }







    override fun observeDataChanges() {

    }


    override fun getLayoutId(): Int  = R.layout.no_internet_dialog

    companion object {

        val TAG = "NoInternetDialogFragment"

        @JvmStatic
        fun newInstance() =
            NoInternetDialogFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

}