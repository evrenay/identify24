package com.evren.common.dialog

interface WebViewFormListener {
    fun onSuccess()
    fun onFailure()
}