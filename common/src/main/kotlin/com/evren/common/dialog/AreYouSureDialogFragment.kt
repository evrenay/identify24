package com.evren.common.dialog

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import com.evren.common.R
import com.evren.common.base.IdentifyBaseDialogFragment
import com.evren.common.databinding.AreYouSureDialogBinding
import com.evren.common.databinding.NoInternetDialogBinding
import javax.inject.Inject


class AreYouSureDialogFragment @Inject constructor() : IdentifyBaseDialogFragment<AreYouSureDialogBinding>() {


    var listener : AreYouSureListenerInterface ?=null

    interface AreYouSureListenerInterface{
        fun onHandleSureData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.yesBtn.setOnClickListener {
            listener?.onHandleSureData()
        }
        binding.noBtn.setOnClickListener {
            dismiss()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (parentFragment is AreYouSureListenerInterface) {
            listener = parentFragment as AreYouSureListenerInterface
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }


    override fun onResume() {
        super.onResume()

        dialog?.setOnKeyListener { dialog, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (event.action == KeyEvent.ACTION_DOWN){
                    true
                }
                else{
                    dismiss()
                    true
                }


            } else
                false
        }
    }





    override fun observeDataChanges() {

    }


    override fun getLayoutId(): Int  = R.layout.are_you_sure_dialog

    companion object {

        val TAG = "AreYouSureDialogFragment"

        @JvmStatic
        fun newInstance() =
            AreYouSureDialogFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

}