package com.evren.core.extensions

import android.widget.EditText
import android.widget.ImageView
import androidx.core.widget.NestedScrollView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.squareup.picasso.Picasso

/**
 * Loads image in given [url] to this [ImageView]
 *
 * @param url url of image
 */
@BindingAdapter("imageUrl")
fun ImageView.loadImage(url: String?) {
    if (!url.isNullOrBlank()) {
        Picasso.get()
            .load(url)
            .into(this)
    }
}


@BindingAdapter("error","scrollView")
fun EditText.error(errorStr : MutableLiveData<String>, scrollView : NestedScrollView?) {
    errorStr.value?.let {
        this.error = it
        scrollView?.post {
            scrollView.scrollTo(0, this.top)
            this.requestFocus()
        }

    }
}
