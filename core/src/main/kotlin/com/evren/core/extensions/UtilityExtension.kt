package com.evren.core.extensions

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.appcompat.widget.SearchView
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import com.evren.core.utils.ClearFocusQueryTextListener

/**
 * Shorthand for [contains] with ignoreCase set [true]
 */
fun CharSequence.containsIgnoreCase(other: CharSequence) = contains(other, true)

/**
 * Adds [ClearFocusQueryTextListener] as [SearchView.OnQueryTextListener]
 */
fun SearchView.setOnQueryChangedListener(block: (String?) -> Unit) = setOnQueryTextListener(ClearFocusQueryTextListener(this, block))

/**
 * Shortening set menu item expands / collapses
 */
fun MenuItem.onExpandOrCollapse(onExpand: () -> Unit, onCollapse: () -> Unit) {
    setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
        override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
            onCollapse()
            return true
        }

        override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
            onExpand()
            return true
        }
    })
}

fun NavController.navigateSafe(
    @IdRes resId: Int,
    args: Bundle? = null,
    navOptions: NavOptions? = null,
    navExtras: Navigator.Extras? = null
) {
    val action = currentDestination?.getAction(resId) ?: graph.getAction(resId)
    if (action != null && currentDestination?.id != action.destinationId) {
        navigate(resId, args, navOptions, navExtras)
    }
}


