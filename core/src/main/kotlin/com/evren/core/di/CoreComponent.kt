package com.evren.core.di

import android.app.Application
import android.content.Context
import android.net.NetworkInfo
import com.evren.core.utils.ResourceProvider
import com.evren.repository.shared.SharedValues
import com.evren.repository.socket.SocketSource
import com.squareup.moshi.Moshi
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [CoreModule::class])
interface CoreComponent {

    fun getAppContext(): Context

    fun getNetworkInfo(): NetworkInfo?

    fun getApplication(): Application

    fun getResourceProvider() : ResourceProvider

    fun getMoshi() : Moshi

    fun getSharedValues() : SharedValues

    fun getSocketSource(): SocketSource



    @Component.Factory
    interface Factory {
        fun create(@BindsInstance app: Application): CoreComponent
    }
}
