package com.evren.core.di

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.evren.repository.shared.SharedValues
import com.evren.repository.socket.SocketSource
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import javax.inject.Provider
import javax.inject.Singleton

@Module
class CoreModule {

    @Provides
    fun proivdeAppContext(app: Application): Context = app.applicationContext

    @Provides
    fun provideNetworkInfo(app: Application): NetworkInfo? =
        (app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo


    @Singleton
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()


    @Singleton
    @Provides
    fun provideSocketSource(context: Context, moshi: Moshi,networkInfoProvider: Provider<NetworkInfo>,sharedValues: SharedValues): SocketSource = SocketSource(context,moshi,networkInfoProvider,sharedValues)


}
