package com.evren.core.base.lifecycle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.evren.core.R
import com.google.android.material.snackbar.Snackbar
import com.evren.repository.interactors.base.Reason
import dagger.android.support.DaggerFragment

/**
 * Parent of all fragments.
 *
 * Purpose of [BaseFragment] is to simplify view creation and provide easy access to fragment's
 * [navController] and [binding].
 */
abstract class BaseFragment<T : ViewDataBinding> : DaggerFragment() {

    //region Abstractions

    @LayoutRes
    abstract fun getLayoutId(): Int
    //endregion

    //region Properties



    private var realBinding: T? = null

    protected val binding: T get() = realBinding ?: throw IllegalStateException("Trying to access the binding outside of the view lifecycle.") as Throwable

    //protected lateinit var binding: T
    //endregion

    //region Functions




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = DataBindingUtil.inflate<T>(inflater, getLayoutId(), container, false).also {
        it.lifecycleOwner = this
        realBinding = it
    }.root

        /*{
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.lifecycleOwner = this
        return binding.root
    }*/

    protected fun showSnackbarWithAction(reason: Reason, block: () -> Unit) {
        Snackbar.make(
            binding.root,
            resources.getString(reason.messageRes),
            Snackbar.LENGTH_INDEFINITE
        ).setAction(R.string.retry) {
            block()
        }.show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        realBinding = null
    }

    //endregion
}
