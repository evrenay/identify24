package com.evren.core.base.lifecycle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.android.support.DaggerDialogFragment

abstract class BaseDialogFragment<T : ViewDataBinding> : DaggerDialogFragment() {

    //region Abstractions

    @LayoutRes
    abstract fun getLayoutId(): Int
    //endregion

    //region Properties

    protected val binding: T get() = realBinding ?: throw IllegalStateException("Trying to access the binding outside of the view lifecycle.")

    private var realBinding: T? = null
    //endregion

    //region Functions

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =  DataBindingUtil.inflate<T>(inflater, getLayoutId(), container, false).also {
        realBinding = it
    }.root


    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun onDestroyView() {
        super.onDestroyView()
        realBinding = null
    }




    //endregion
}