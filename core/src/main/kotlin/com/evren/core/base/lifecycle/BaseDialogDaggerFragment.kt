package com.evren.core.base.lifecycle


import android.content.Context
import androidx.databinding.ViewDataBinding
import com.evren.core.di.ViewModelFactory
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseDialogDaggerFragment<T : ViewDataBinding> : BaseDialogFragment<T>(), HasAndroidInjector {

    //region Properties

    @get:Inject
    internal var androidInjector: DispatchingAndroidInjector<Any>? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    //endregion

    //region Functions

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }



    //override fun androidInjector(): AndroidInjector<Any>? = androidInjector
    //endregion
}