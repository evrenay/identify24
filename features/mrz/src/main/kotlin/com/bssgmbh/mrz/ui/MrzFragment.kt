package com.bssgmbh.mrz.ui

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bssgmbh.mrz.R
import com.bssgmbh.mrz.databinding.FragmentMrzBinding
import com.bssgmbh.mrz.ui.mlkit.camera.CameraSource
import com.bssgmbh.mrz.ui.mlkit.text.TextRecognitionProcessor
import com.evren.common.base.IdentifyBaseFragment
import com.evren.core.extensions.navigateSafe
import com.evren.repository.model.mrz.DocType
import org.jmrtd.lds.icao.MRZInfo
import java.io.IOException
import javax.inject.Inject

class MrzFragment @Inject constructor() : IdentifyBaseFragment<FragmentMrzBinding>() ,
    TextRecognitionProcessor.ResultListener {

    var cameraSource : CameraSource ?= null
    var docType : DocType = DocType.ID_CARD


    private val viewModel by viewModels<MrzViewModel>{viewModelFactory}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createCameraSource()
    }


    override fun observeDataChanges() {

    }

    override fun getLayoutId(): Int  = R.layout.fragment_mrz

    private fun createCameraSource() {
        if (cameraSource == null) {
            cameraSource = CameraSource(activity, binding.graphicsOverlay)
            cameraSource?.setFacing(CameraSource.CAMERA_FACING_BACK)
        }
        cameraSource?.setMachineLearningFrameProcessor(TextRecognitionProcessor(docType, this))
    }

    private fun startCameraSource() {
        if (cameraSource != null) {
            try {
                binding.cameraSourcePreview.start(cameraSource, binding.graphicsOverlay)
            } catch (e: IOException) {
                cameraSource?.release()
                cameraSource = null
            }
        }
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        binding.cameraSourcePreview.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (cameraSource != null) {
            cameraSource?.release()
        }
    }


    override fun onSuccess(mrzInfo: MRZInfo?) {
       findNavController().navigateSafe(R.id.action_mrzFragment_to_nfcFragment, bundleOf("mrzInfo" to mrzInfo,"identId" to viewModel.identId))
    }

    override fun onError(exp: Exception?) {

    }

}