package com.bssgmbh.mrz.ui

import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.LottieDrawable
import com.bssgmbh.mrz.R
import com.bssgmbh.mrz.databinding.FragmentNfcBinding
import com.evren.common.actions.openCall
import com.evren.common.base.IdentifyBaseDialogFragment
import com.evren.core.extensions.observe
import com.evren.repository.model.dto.MrzDto
import com.evren.repository.model.enum.NavGraph
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jmrtd.BACKey
import org.jmrtd.BACKeySpec
import org.jmrtd.lds.icao.MRZInfo
import javax.inject.Inject

class NfcFragment @Inject constructor() : IdentifyBaseDialogFragment<FragmentNfcBinding>() {

    private val viewModel by viewModels<NfcViewModel> { viewModelFactory }

    var finishedNfcProcess : () -> Unit = { }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            viewModel.mrzInfo = it.getSerializable("mrzInfo") as MRZInfo
            viewModel.identId = it.getString("identId")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewTitle.ivBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }






    fun triggerNfcReader(isoDep: IsoDep){
        startNfcReading()
        val bacKey: BACKeySpec = BACKey(viewModel.mrzInfo?.documentNumber, viewModel.mrzInfo?.dateOfBirth, viewModel.mrzInfo?.dateOfExpiry)
        viewModel.getNfcData(isoDep,bacKey)
    }

    private fun startNfcReading() {
        binding.linLayScannerReady.visibility = View.GONE
        binding.nfcStatusTv.text = getString(R.string.nfc_reading)
        binding.nfcAnimation.setAnimation(R.raw.nfc_reading)
        binding.nfcAnimation.repeatCount = LottieDrawable.INFINITE
        binding.nfcAnimation.playAnimation()
    }
    private fun finishedNfcReading(isSuccess : Boolean) {
       when(isSuccess){
           true->{
               binding.nfcAnimation.setAnimation(R.raw.nfc_success)
               binding.nfcStatusTv.text = getString(R.string.nfc_success)
           }
           false->{
               binding.nfcAnimation.setAnimation(R.raw.nfc_fail)
               binding.nfcStatusTv.text = getString(R.string.nfc_try_again)
           }
       }
        binding.nfcAnimation.repeatCount = 0
        binding.nfcAnimation.playAnimation()
    }


    override fun observeDataChanges() {
        observe(viewModel.errorData){
            Toast.makeText(requireContext(),getString(R.string.nfc_toast_message),Toast.LENGTH_SHORT).show()
            finishedNfcProcess()
            finishedNfcReading(false)
        }
        observe(viewModel.successData){
            finishedNfcProcess()
            finishedNfcReading(true)
            lifecycleScope.launch {
                delay(3000)
                viewModel.sharedValues.changeNavGraph.value = NavGraph.CALL.name
            }
        }
        observe(viewModel.errorMrz){
            Toast.makeText(requireContext(),getString(R.string.nfc_toast_message),Toast.LENGTH_SHORT).show()
            finishedNfcProcess()
            finishedNfcReading(false)
        }

    }



    override fun getLayoutId(): Int = R.layout.fragment_nfc


}