package com.bssgmbh.mrz.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class MrzScope
