package com.bssgmbh.mrz.di.modules

import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.navArgs
import com.bssgmbh.mrz.ui.MrzFragment
import com.bssgmbh.mrz.ui.MrzFragmentArgs
import com.bssgmbh.mrz.ui.MrzViewModel
import com.bssgmbh.mrz.ui.NfcViewModel
import com.evren.core.di.keys.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
@ExperimentalCoroutinesApi
abstract class MrzNfcModule {

    //region ViewModels

    @Binds
    @IntoMap
    @ViewModelKey(MrzViewModel::class)
    abstract fun mrzViewModel(mrzViewModel: MrzViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NfcViewModel::class)
    abstract fun nfcViewModel(nfcViewModel: NfcViewModel): ViewModel


    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideIdentId(fragment: MrzFragment): String {
            val args by fragment.navArgs<MrzFragmentArgs>()
            return args.identId
        }
    }
}
