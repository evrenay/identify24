package com.bssgmbh.mrz.di

import com.bssgmbh.mrz.di.modules.MrzNfcModule
import com.bssgmbh.mrz.di.scopes.MrzScope
import com.bssgmbh.mrz.ui.MrzFragment
import com.bssgmbh.mrz.ui.NfcFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi

/**
 * Contributes fragments & view models in this module
 */
@Module
@ExperimentalCoroutinesApi
abstract class MrzContributor {

    //region Contributes

    @ContributesAndroidInjector(
        modules = [MrzNfcModule::class]
    )
    @MrzScope
    abstract fun mrzFragment(): MrzFragment

    @ContributesAndroidInjector(
        modules = [MrzNfcModule::class]
    )
    @MrzScope
    abstract fun nfcFragment(): NfcFragment



    //endregion
}
