package com.evren.form.di


import com.evren.common.dialog.WebViewFormFragment
import com.evren.form.di.modules.FormFragmentModule
import com.evren.form.di.scopes.FormScope
import com.evren.form.ui.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Contributes fragments & view models in this module
 */
@Module
abstract class FormContributor {

    //region Contributes

    @ContributesAndroidInjector(
        modules = [FormFragmentModule::class]
    )
    @FormScope
    abstract fun formOneFragment(): FormOneFragment
    @ContributesAndroidInjector(
        modules = [FormFragmentModule::class]
    )
    @FormScope
    abstract fun formTwoFragment(): FormTwoFragment

    @ContributesAndroidInjector(
        modules = [FormFragmentModule::class]
    )
    @FormScope
    abstract fun formThreeFragment(): FormThreeFragment

    @ContributesAndroidInjector(
        modules = [FormFragmentModule::class]
    )
    @FormScope
    abstract fun formFourFragment(): FormFourFragment

    @ContributesAndroidInjector(
        modules = [FormFragmentModule::class]
    )
    @FormScope
    abstract fun webViewFormFragment(): WebViewFormFragment



    //endregion
}
