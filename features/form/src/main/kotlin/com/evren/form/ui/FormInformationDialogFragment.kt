package com.evren.form.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.evren.form.R

class FormInformationDialogFragment : DialogFragment() {

    @DrawableRes
    private var resourceId : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FormInformationDialogStyle)
        arguments?.let {

            resourceId = it.getInt("resourceId")
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        isCancelable = true
        val view = inflater.inflate(R.layout.fragment_form_information,container,false)
        val infoImg = view.findViewById<ImageView>(R.id.infoImg)
        val backBtn = view.findViewById<ImageButton>(R.id.closeBtn)
        infoImg.setImageDrawable(ContextCompat.getDrawable(activity!!.baseContext,resourceId!!))

        backBtn.setOnClickListener {
            dismiss()
        }



        return view

    }



    companion object {

        @JvmStatic
        fun newInstance(@DrawableRes resourceId : Int) =
            FormInformationDialogFragment().apply {
                arguments = Bundle().apply {

                        putInt("resourceId",resourceId)
                }
            }
    }
}