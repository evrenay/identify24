package com.evren.form.di.modules

import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.navArgs
import com.evren.core.di.keys.ViewModelKey
import com.evren.form.ui.FormOneFragment
import com.evren.form.ui.FormOneFragmentArgs
import com.evren.form.ui.FormViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class FormFragmentModule {

    //region ViewModels

    @Binds
    @IntoMap
    @ViewModelKey(FormViewModel::class)
    abstract fun formViewModel(formViewModel: FormViewModel): ViewModel
    //endregion

    @Module
    companion object {

        /**
         * Provides launch detail params
         */
        @Provides
        @JvmStatic
        fun provideGetIdentIdParams(fragment: FormOneFragment): String {
            val args by fragment.navArgs<FormOneFragmentArgs>()
            return args.indentId
        }

    }
}
