package com.evren.form.ui

import android.os.Bundle
import android.view.View
import androidx.annotation.DrawableRes
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.evren.common.base.IdentifyBaseFragment
import com.evren.core.extensions.observe
import com.evren.form.R
import com.evren.form.databinding.FormFourBinding

class FormFourFragment : IdentifyBaseFragment<FormFourBinding>() {


    //region Properties
    private val viewModel by viewModels<FormViewModel> { viewModelFactory }

    //endregion

    //region Functions

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        // Observing error to show toast with retry action

        binding.backBtn.setOnClickListener{
            it.findNavController().popBackStack(R.id.formThreeFragment,false)
        }

        infoImgActions()
    }

    override fun observeDataChanges() {
        observe(viewModel.errorData) {
            showSnackbarWithAction(it) {
                viewModel.retry()
            }
        }
    }


    private fun infoImgActions(){
        binding.typeOfIdCardInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.art_des_ausweises)
        }
        binding.cardNumberInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.ausweisnummer)
        }
        binding.dateOfIssueInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.ausstellungsdatum)
        }
        binding.effectiveDateInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.gultigkeitsdatum)
        }
        binding.issuingAuthorityInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.ausstellende_behorde)
        }

    }


    private fun openInfoImgFragment(@DrawableRes resourceId : Int){
        val goInfoImg = FormInformationDialogFragment.newInstance(resourceId)
        goInfoImg.show(childFragmentManager,"form-info-image")
    }





    override fun getLayoutId(): Int = R.layout.fragment_form_four

    //endregion
}