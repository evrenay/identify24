package com.evren.form.ui

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.nfc.NfcAdapter
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.evren.common.actions.openCall
import com.evren.common.actions.openMrz
import com.evren.common.base.IdentifyBaseFragment
import com.evren.common.dialog.WebViewFormFragment
import com.evren.common.dialog.WebViewFormListener
import com.evren.core.extensions.observe
import com.evren.form.R
import com.evren.form.databinding.FormOneBinding
import com.evren.repository.interactors.base.ApiError
import com.evren.repository.interactors.base.State
import com.evren.repository.model.enum.FormStatusType
import es.dmoral.toasty.Toasty
import permissions.dispatcher.*
import java.util.*
import javax.inject.Inject


@RuntimePermissions
class FormOneFragment @Inject constructor() : IdentifyBaseFragment<FormOneBinding>(), WebViewFormListener {

    //region Properties

    private val viewModel by viewModels<FormViewModel> { viewModelFactory }

    var webViewFormFragment : WebViewFormFragment?= null

    //endregion

    //region Functions



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel
        setLanguageIconOnToolbar()
        hideProgress()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewTitle.ivBack.visibility = View.GONE

        // Observing error to show toast with retry action
        binding.relLayCallWaitingFragmentBtn.setOnClickListener {
            showCameraWithPermissionCheck()
        }

        binding.viewTitle.ivLanguage.setOnClickListener {
            showChangeLanguage()
        }

    }





    private fun showChangeLanguage(){
        val languageList = arrayOf<CharSequence>(getString(R.string.english),getString(R.string.german),getString(R.string.turkish))
        val alert = AlertDialog.Builder(requireContext())
        alert.setTitle(getString(R.string.choose_language))
        alert.setSingleChoiceItems(languageList,viewModel.selectedLanguageIndex,DialogInterface.OnClickListener { dialogInterface, i ->
            when(i){
                0->{
                    setLocale("en")
                }
                1->{
                    setLocale("")
                }
                2->{
                    setLocale("tr")
                }
            }
            dialogInterface.dismiss()
        })
        alert.create().show()
    }

    private fun setLocale(language: String) {
       viewModel.sharedValues.changeLanguage.value = language
    }

    private fun setLanguageIconOnToolbar(){
        when(Locale.getDefault().language){
            "en"->{
                binding.viewTitle.ivLanguage.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_united_kingdom))
                viewModel.selectedLanguageIndex = 0
            }
            "tr"->{
                binding.viewTitle.ivLanguage.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_turkey))
                viewModel.selectedLanguageIndex = 2
            }
            "de"->{
                binding.viewTitle.ivLanguage.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_germany))
                viewModel.selectedLanguageIndex = 1
            }
            ""->{
                binding.viewTitle.ivLanguage.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_germany))
                viewModel.selectedLanguageIndex = 1
            }
            else->{
                binding.viewTitle.ivLanguage.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_united_kingdom))
                viewModel.selectedLanguageIndex = 2
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated method
        onRequestPermissionsResult(requestCode, grantResults)
    }


    override fun observeDataChanges() {

        observe(viewModel.errorData) {
            hideProgress()
            when (it) {
                is ApiError -> {
                    it.message?.get(0)?.let { it1 ->
                        Toasty.error(context!!,
                            it1,
                            Toast.LENGTH_SHORT,
                            true).show()
                    }
                }
                else -> {
                    Toasty.error(context!!,
                        it.messageRes,
                        Toast.LENGTH_SHORT,
                        true).show()
                }
            }

        }

        observe(viewModel.stateData) { state ->
            when (state) {
                is State.Loading -> {
                    binding.loadingLayer.relLayLoading.visibility = View.VISIBLE
                    binding.relLayCallWaitingFragmentBtn.isEnabled = false
                }
            }
        }

        observe(viewModel.sharedValues.customerInformation){
            it?.formUid?.let { id ->
                viewModel.identId.value = id
            }
        }


        observe(viewModel.customerInformation) {
            it.status?.let { id->
                when(id){
                    FormStatusType.WEB_VIEW.type->{
                        openWebViewFragment()
                    }
                    FormStatusType.GO_MRZ.type->{
                        checkNfcExisting({
                            it.formUid?.let { identId-> openMrz(identId) }
                        },{
                            openWebViewFragment()
                        })
                    }
                    else->{
                            openWebViewFragment()
                    }
                }
            }

        }
    }


    private fun hideProgress(){
        binding.loadingLayer.relLayLoading.visibility = View.GONE
        binding.relLayCallWaitingFragmentBtn.isEnabled = true
    }

    private fun checkNfcExisting(itsOnDevice : () -> Unit, notOnDevice : () -> Unit ){
        val adapter = NfcAdapter.getDefaultAdapter(activity)
        if (adapter == null) notOnDevice() else itsOnDevice()
    }

    override fun onDestroy() {
        super.onDestroy()
        webViewFormFragment?.dismiss()
        webViewFormFragment = null
    }


     fun openWebViewFragment(){
         viewModel.sharedValues.customerInformation.value?.webviewUrl?.let { url ->
             if (webViewFormFragment == null) webViewFormFragment = WebViewFormFragment.newInstance(url)
             childFragmentManager.beginTransaction().add(
                     webViewFormFragment!!,
                     WebViewFormFragment::class.java.toString()).commitAllowingStateLoss()
         }
    }



    @NeedsPermission(Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO)
     fun showCamera() {
             viewModel.startCall()
        }

    @OnShowRationale(Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO)
    fun showRationaleForCamera(request: PermissionRequest) {
        showRationaleDialog(R.string.permission_camera_rationale, request)
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO)
    fun onCameraNeverAskAgain() {
       openPermissionSettings()
    }

    private fun openPermissionSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", activity?.packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    /*     binding.nextFragmentBtn.setOnClickListener {
             if (!viewModel.identId.isNullOrEmpty()){
                 val action = FormOneFragmentDirections.nextAction().setFragmentOne(1234)
                 this.findNavController().navigate(action)
             }
         }*/


    private fun showRationaleDialog(@StringRes messageResId: Int, request: PermissionRequest) {
        AlertDialog.Builder(context!!)
            .setPositiveButton(R.string.button_allow) { _, _ -> request.proceed() }
            .setNegativeButton(R.string.button_deny) { _, _ -> request.cancel() }
            .setCancelable(false)
            .setMessage(messageResId)
            .show()
    }




    override fun getLayoutId(): Int = R.layout.fragment_form_one

    override fun onSuccess() {
        webViewFormFragment?.let {
            it.dismiss()
            webViewFormFragment = null
        }
        viewModel.startCall()
    }

    override fun onFailure() {
        webViewFormFragment?.let {
            if (it.isAdded) it.dismiss()
            webViewFormFragment = null
            hideProgress()
        }
    }

    //endregion

    companion object{
        var NO_LANGUAGE = "no_language"
    }

}