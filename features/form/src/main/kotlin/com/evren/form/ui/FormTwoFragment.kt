package com.evren.form.ui

import android.os.Bundle
import android.view.View
import androidx.annotation.DrawableRes
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.evren.common.base.IdentifyBaseFragment
import com.evren.core.extensions.observe
import com.evren.form.R
import com.evren.form.databinding.FormTwoBinding

class FormTwoFragment : IdentifyBaseFragment<FormTwoBinding>() {

    //region Properties
    private val viewModel by viewModels<FormViewModel> { viewModelFactory }

    //endregion

    //region Functions


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

         binding.viewModel = viewModel

        // Observing error to show toast with retry action


        binding.backBtn.setOnClickListener{
            it.findNavController().popBackStack(R.id.formOneFragment,false)
        }


        binding.goNextBtn.setOnClickListener {

            val action = FormTwoFragmentDirections.nextAction()
            this.findNavController().navigate(action)

        }


        infoImgActions()


    }

    override fun observeDataChanges() {
        observe(viewModel.errorData) {
            showSnackbarWithAction(it) {
                viewModel.retry()
            }
        }
    }




    private fun infoImgActions(){
        binding.nameInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.vorname)
        }
        binding.surNameInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.nachname)
        }
        binding.dayOfBirthInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.geburtsdatum)
        }
        binding.placeOfBirthInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.geburtsort)
        }
        binding.nationalityInfoImg.setOnClickListener {
            openInfoImgFragment(R.drawable.staatsangehorigkeit)
        }

    }

    private fun openInfoImgFragment(@DrawableRes resourceId : Int){
        val goInfoImg = FormInformationDialogFragment.newInstance(resourceId)
        goInfoImg.show(childFragmentManager,"form-info-image")
    }


    override fun getLayoutId(): Int = R.layout.fragment_form_two

    //endregion

}