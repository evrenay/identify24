package com.evren.form.ui


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.evren.core.base.viewmodel.BaseViewModel
import com.evren.core.utils.ResourceProvider
import com.evren.form.R
import com.evren.repository.model.FormEntity
import com.evren.repository.interactors.customer.GetCustomerInformation
import com.evren.repository.interactors.base.handle
import com.evren.repository.model.CustomerIdDto
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.shared.SharedValues
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class FormViewModel@Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val getCustomerInformation : GetCustomerInformation,
    private val getIdentIdParams : String,
    val sharedValues: SharedValues
): BaseViewModel<FormEntity>() {


    var selectedLanguageIndex: Int  = 0
    val identId = MutableLiveData<String>()
    val identIdError = MutableLiveData<String>()
    val customerInformation = MutableLiveData<CustomerInformationEntity>()
    fun isEmptyIdentId() = identId.value.isNullOrEmpty()


    fun startCall(){
        checkIdentId {
            viewModelScope.launch {
                getCustomerInformation(GetCustomerInformation.Params(CustomerIdDto(identId.value!!))).collect {
                    it.handle(::handleState,::handleFailure){successData ->
                        sharedValues.customerInformation.value = successData
                        customerInformation.value = successData
                    }
                }
            }
        }
    }



    fun checkIdentId(success : () -> Unit){
        if (isEmptyIdentId()){
            identIdError.value = resourceProvider.getString(R.string.ident_id_empty_error)
            return
        }
        success()
    }


    override suspend fun loadData() {
        if (!getIdentIdParams.equals("null") && !getIdentIdParams.trim().isEmpty()){
            identId.value = getIdentIdParams
        }
    }
}