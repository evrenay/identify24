package com.evren.webrtc.ui

import android.content.Context
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.evren.common.base.IdentifyBaseFragment
import com.evren.core.extensions.navigateSafe
import com.evren.core.extensions.observe
import com.evren.repository.model.SocketActionType
import com.evren.repository.model.enum.SocketConnectionStatus
import com.evren.webrtc.R
import com.evren.webrtc.databinding.CallingBinding
import es.dmoral.toasty.Toasty
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
class CallingFragment : IdentifyBaseFragment<CallingBinding>()    {


    //region Properties


    private val viewModel by viewModels<CallingViewModel> { viewModelFactory }

    var mediaPlayer : MediaPlayer ?= null

    //endregion





    //region Functions

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        setMediaPlayer()
     /*   binding.buttonRejectCall.setOnClickListener {
            viewModel.finishCall()
            findNavController().popBackStack()
        }*/
        binding.buttonAcceptCall.setOnClickListener {
            findNavController().navigateSafe(R.id.action_callingFragment_to_startedCallFragment)
        }

        onBackPressClicked()

    }


    private fun onBackPressClicked(){
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                return@OnKeyListener true
            }
            false
        })
    }


    private fun setMediaPlayer(){
        mediaPlayer = MediaPlayer.create(requireContext(),R.raw.call_ring)
        mediaPlayer?.isLooping = true
        mediaPlayer?.start()

    }

  /*  fun startVibration(){
         vibrator = context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator?.vibrate(VibrationEffect.createOneShot(200, 10))
        } else {
            vibrator?.vibrate(200)
        }
    }*/




    override fun observeDataChanges() {
        observe(viewModel.successData){
            when(it.action){
                SocketActionType.TERMINATE_CALL.type->{
                    findNavController().popBackStack()
                }
                SocketActionType.SUBREJECTED.type->{
                    findNavController().popBackStack()
                }
                SocketActionType.IM_ONLINE.type -> {
                    //Toast.makeText(context,getString(R.string.customer_service_online), Toast.LENGTH_LONG).show()
                }
                SocketActionType.IM_OFFLINE.type -> {
                    findNavController().popBackStack()
                    //Toast.makeText(context,getString(R.string.customer_service_offline), Toast.LENGTH_LONG).show()
                }
                SocketActionType.END_CALL.type->{
                    findNavController().popBackStack()
                }
            }
        }

        observe(viewModel.errorData) {
            errorProccess()
            viewModel.errorData.removeObservers(this)
        }

        observe(viewModel.sharedValues.socketStatus){
            when(it){
                SocketConnectionStatus.CLOSE.type->{
                    errorProccess()
                    viewModel.sharedValues.socketStatus.removeObservers(this)
                }
            }
        }




    }


    fun errorProccess(){
        Toasty.error(context!!,getString(R.string.connection_error_when_calling),Toast.LENGTH_LONG).show()
        findNavController().popBackStack()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        mediaPlayer?.stop()
        mediaPlayer = null
    }



    override fun onResume() {
        super.onResume()
        viewModel.connectSocket()
    }



    override fun getLayoutId(): Int = R.layout.fragment_calling

    //endregion
}