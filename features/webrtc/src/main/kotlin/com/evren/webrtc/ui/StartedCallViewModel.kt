package com.evren.webrtc.ui

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.evren.core.base.viewmodel.BaseViewModel
import com.evren.repository.interactors.SocketConnectInteractor
import com.evren.repository.interactors.base.None
import com.evren.repository.interactors.base.handle
import com.evren.repository.interactors.base.onFailure
import com.evren.repository.interactors.tan.SetSmsCode
import com.evren.repository.model.SocketActionType
import com.evren.repository.model.dto.TanDto
import com.evren.repository.model.entities.TanEntity
import com.evren.repository.model.enum.RtcMessageType
import com.evren.repository.model.enum.SdpType
import com.evren.repository.model.socket.FlashToggle
import com.evren.repository.model.socket.SocketResponse
import com.evren.repository.model.socket.TanResponse
import com.evren.repository.shared.SharedValues
import com.evren.repository.socket.RtcConnectionSource
import com.evren.webrtc.ui.StartedCallFragment.Companion.CAMERA_BACK
import com.evren.webrtc.ui.StartedCallFragment.Companion.CAMERA_FRONT
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class StartedCallViewModel
@Inject constructor(
    val socketConnection : SocketConnectInteractor,
    val sharedValues: SharedValues,
    val setSmsCode : SetSmsCode
) : BaseViewModel<SocketResponse>() {


    val tanResponse = MutableLiveData<TanEntity>()
    var tanId : String ?= null
    var tanCode : String ?= null
    var currentCamera = CAMERA_FRONT
    var isTorchOn = false


    fun startRtcProccessing(){
        viewModelScope.launch {
            sharedValues.customerInformation.value?.customerUid?.let {
                socketConnection.sendStartCall(it)
                    .handle(::handleState, ::handleFailure, ::handleSuccess)
            }
        }

    }

    fun setSmsCode(){
        tanId?.let {tid ->
            tanCode?.let { code ->
                viewModelScope.launch {
                    setSmsCode(SetSmsCode.Params(TanDto(tid,code))).collect {
                        it.handle(::handleState,::handleFailure){ response ->
                            tanCodeResultWillSendWithSocket()
                            tanResponse.value = response
                        }
                    }
                }
            }
        }
    }

    fun tanCodeResultWillSendWithSocket(){
        sharedValues.customerInformation.value?.customerUid?.let { room ->
            tanId?.let { tanId ->
                tanCode?.let {code ->
                    viewModelScope.launch {
                        socketConnection.sendTanResponse(TanResponse(room = room,tid = tanId,tan = code))?.onFailure {
                            handleFailure(it)
                        }
                    }
                }
            }

        }

    }




    fun handleSdpMessage(socketResponse : SocketResponse) {
        when (socketResponse.action) {
            SocketActionType.SDP.type->{
                when(socketResponse.sdp?.type){
                    SdpType.READY.type -> {

                    }
                    SdpType.OFFER.type -> {

                    }
                    SdpType.ANSWER.type -> {

                    }
                }
            }
            SocketActionType.CANDIDATE.type->{

            }
        }


    }







    override suspend fun loadData() {
        sharedValues.customerInformation.value?.customerUid?.let {
            socketConnection(None()).collect {
                it.handle(::handleState,::handleFailure,::handleSuccess)
            }
        }
    }

    fun closeStream() {
        socketConnection.rtcConnectionSource.dispose()
    }

    fun enableCamera(enabled: Boolean) {
        socketConnection.rtcConnectionSource.cameraEnabled = enabled
    }

    fun enableMicrophone(enabled: Boolean) {
       socketConnection.rtcConnectionSource.microphoneEnabled = enabled
    }

    fun switchCamera(){
        socketConnection.rtcConnectionSource.switchCamera()
        currentCamera = if (currentCamera == CAMERA_FRONT) CAMERA_BACK else CAMERA_FRONT
    }


     fun switchFlash(result: Boolean){
        viewModelScope.launch {
            socketConnection.sendFlashToggle(FlashToggle(result = result))?.onFailure {
                handleFailure(it)
            }
        }

    }


}