package com.evren.webrtc.di.modules

import androidx.lifecycle.ViewModel
import com.evren.core.di.keys.ViewModelKey
import com.evren.webrtc.ui.CallWaitingViewModel
import com.evren.webrtc.ui.CallingViewModel
import com.evren.webrtc.ui.StartedCallViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
@ExperimentalCoroutinesApi
abstract class CallFragmentModule {

    //region ViewModels

    @Binds
    @IntoMap
    @ViewModelKey(CallWaitingViewModel::class)
    abstract fun callWaitingViewModel(callWaitingViewModel: CallWaitingViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(CallingViewModel::class)
    abstract fun callingViewModel(callingViewModel: CallingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StartedCallViewModel::class)
    abstract fun startedCallViewModel(startedCallViewModel: StartedCallViewModel): ViewModel
    //endregion

    @Module
    companion object {

    }
}
