package com.evren.webrtc.ui

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.evren.common.base.IdentifyBaseFragment
import com.evren.common.dialog.AreYouSureDialogFragment
import com.evren.core.extensions.navigateSafe
import com.evren.core.extensions.observe
import com.evren.repository.model.SocketActionType
import com.evren.repository.model.enum.NavGraph
import com.evren.repository.model.enum.SocketConnectionStatus
import com.evren.webrtc.R
import com.evren.webrtc.databinding.WaitingCallBinding
import es.dmoral.toasty.Toasty
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
class CallWaitingFragment : IdentifyBaseFragment<WaitingCallBinding>(),
    AreYouSureDialogFragment.AreYouSureListenerInterface {


    //region Properties

    private var sureDialogFragment : AreYouSureDialogFragment ?= null

    private val viewModel by viewModels<CallWaitingViewModel> { viewModelFactory }

    //endregion





    //region Functions

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel


        binding.viewTitle.ivBack.setOnClickListener {
            showSureFragment()
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                showSureFragment()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        binding.btnReConnect.setOnClickListener {
            if (isNetworkConnected()){
                binding.btnReConnect.isEnabled = false
                viewModel.connectSocket()

            } else Toasty.error(context!!,getString(R.string.reason_network),Toast.LENGTH_SHORT).show()


        }

    }

    override fun onResume() {
        super.onResume()
        viewModel.connectSocket()
    }








    fun showSureFragment(){
        if (sureDialogFragment == null) sureDialogFragment =  AreYouSureDialogFragment.newInstance()
        sureDialogFragment?.let {
            childFragmentManager.beginTransaction().add(it,AreYouSureDialogFragment::class.java.toString()).commitAllowingStateLoss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        sureDialogFragment?.let {
            it.dismissAllowingStateLoss()
        }
        sureDialogFragment = null
    }




    override fun observeDataChanges() {
        observe(viewModel.successData){
            when(it.action){
                SocketActionType.INIT_CALL.type->{
                    findNavController().navigateSafe(R.id.action_waitingCallFragment_to_callingFragment)
                }
                SocketActionType.IM_ONLINE.type -> {
               // Toast.makeText(context,getString(R.string.customer_service_online),Toast.LENGTH_LONG).show()
                }
                SocketActionType.IM_OFFLINE.type -> {
                   // Toast.makeText(context,getString(R.string.customer_service_offline),Toast.LENGTH_LONG).show()
                }
            }
        }

        observe(viewModel.errorData) {

                    binding.linLayConnectionLost.visibility = View.VISIBLE
                    binding.linLayConnectionSuccess.visibility = View.GONE
                    binding.btnReConnect.isEnabled = true

        }

        observe(viewModel.sharedValues.socketStatus){
            when(it){
                SocketConnectionStatus.OPEN.type->{
                    binding.btnReConnect.isEnabled = true
                    binding.linLayConnectionSuccess.visibility = View.VISIBLE
                    binding.linLayConnectionLost.visibility = View.GONE
                }
                SocketConnectionStatus.CLOSE.type->{
                    binding.linLayConnectionLost.visibility = View.VISIBLE
                    binding.linLayConnectionSuccess.visibility = View.GONE
                    binding.btnReConnect.isEnabled = true
                }
                SocketConnectionStatus.EXCEPTION.type->{
                    Toasty.error(context!!,getString(R.string.reason_network),Toast.LENGTH_SHORT).show()
                    binding.btnReConnect.isEnabled = true
                }
            }
        }
    }


    override fun getLayoutId(): Int = R.layout.fragment_waiting_call


    override fun onHandleSureData() {
        sureDialogFragment?.let {
            it.dismiss()
            viewModel.disconnectSocket()
            viewModel.sharedValues.changeNavGraph.value = NavGraph.FORM.name
        }


    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager = context?.applicationContext!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager?.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    //endregion

}