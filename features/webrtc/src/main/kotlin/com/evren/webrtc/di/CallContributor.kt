package com.evren.webrtc.di

import com.evren.webrtc.di.modules.CallFragmentModule
import com.evren.webrtc.di.scopes.CallScope
import com.evren.webrtc.ui.CallWaitingFragment
import com.evren.webrtc.ui.CallingFragment
import com.evren.webrtc.ui.StartedCallFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi

/**
 * Contributes fragments & view models in this module
 */
@Module
@ExperimentalCoroutinesApi
abstract class CallContributor {

    //region Contributes

    @ContributesAndroidInjector(
        modules = [CallFragmentModule::class]
    )
    @CallScope
    abstract fun callWaitingFragment(): CallWaitingFragment

    @ContributesAndroidInjector(
        modules = [CallFragmentModule::class]
    )
    @CallScope
    abstract fun callingFragment(): CallingFragment

    @ContributesAndroidInjector(
        modules = [CallFragmentModule::class]
    )
    @CallScope
    abstract fun startedCallFragment(): StartedCallFragment


    //endregion
}
