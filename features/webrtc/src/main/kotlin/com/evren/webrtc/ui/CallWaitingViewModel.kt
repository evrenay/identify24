package com.evren.webrtc.ui

import androidx.lifecycle.viewModelScope
import com.evren.core.base.viewmodel.BaseViewModel
import com.evren.repository.interactors.SocketConnectInteractor
import com.evren.repository.interactors.base.*
import com.evren.repository.model.socket.SocketResponse
import com.evren.repository.shared.SharedValues
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject



@ExperimentalCoroutinesApi
class CallWaitingViewModel
@Inject constructor(
    val socketConnection: SocketConnectInteractor,
    val sharedValues: SharedValues
   ) : BaseViewModel<SocketResponse>() {





     fun connectSocket(){
         viewModelScope.launch {
             sharedValues.customerInformation.value?.customerUid?.let {
                 socketConnection(None()).collect {
                     it.handle(::handleState,::handleFailure,::handleSuccess)
                 }
             }
         }
     }

    fun disconnectSocket(){
        socketConnection.closeSocketStream()
    }





    override suspend fun loadData() {

    }



}