package com.evren.webrtc.ui

import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.hardware.Camera
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.evren.common.base.IdentifyBaseFragment
import com.evren.core.extensions.observe
import com.evren.repository.interactors.base.ApiError
import com.evren.repository.interactors.base.State
import com.evren.repository.model.SocketActionType
import com.evren.repository.model.enum.SdpType
import com.evren.repository.model.enum.SocketConnectionStatus
import com.evren.webrtc.R
import com.evren.webrtc.databinding.FragmentBottomTanBinding
import com.evren.webrtc.databinding.StartedCallBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import es.dmoral.toasty.Toasty
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.webrtc.EglBase
import org.webrtc.RendererCommon


@ExperimentalCoroutinesApi
class StartedCallFragment : IdentifyBaseFragment<StartedCallBinding>()    {


    //region Properties

    private val viewModel by viewModels<StartedCallViewModel> { viewModelFactory }

    private var rootEglBase: EglBase ?= null

    var mBottomSheetDialog : BottomSheetDialog ? = null

    var mBehavior : BottomSheetBehavior<View> ? = null

    var bottomBinding: FragmentBottomTanBinding ?= null

    var cam :Camera ?= null

    var mCameraManager:CameraManager? = null



    //endregion





    //region Functions

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        mCameraManager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        initSurfaces()
        viewModel.socketConnection.rtcConnectionSource.apply {
            rootEglBase = this@StartedCallFragment.rootEglBase
            surfaceViewRendererLocal = binding.surfaceViewRendererLocal
            surfaceViewRendererRemote = binding.surfaceViewRendererRemote
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            binding.surfaceViewRendererRemote.visibility = View.VISIBLE
        }



        binding.cameraEnabledToggle.setOnCheckedChangeListener { _, enabled ->
            viewModel.enableCamera(!enabled)
        }

        binding.microphoneEnabledToggle.setOnCheckedChangeListener { _, enabled ->
            viewModel.enableMicrophone(!enabled)
        }

        binding.disconnectButton.setOnClickListener {
            findNavController().popBackStack()
        }

        onBackPressClicked()
    }


    private fun onBackPressClicked(){
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                Toasty.info(requireContext(),R.string.identify_is_in_progress,Toast.LENGTH_SHORT).show()
                return@OnKeyListener true
            }
            false
        })
    }


    private fun initSurfaces() {
        rootEglBase = EglBase.create()
        binding.surfaceViewRendererLocal.init(rootEglBase?.eglBaseContext, null)
        binding.surfaceViewRendererLocal.setEnableHardwareScaler(true)
        binding.surfaceViewRendererLocal.setMirror(false)
        binding.surfaceViewRendererLocal.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)

        binding.surfaceViewRendererRemote.init(rootEglBase?.eglBaseContext, null)
        binding.surfaceViewRendererRemote.setEnableHardwareScaler(true)
        binding.surfaceViewRendererRemote.setMirror(false)
        binding.surfaceViewRendererRemote.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
    }

    override fun onStop() {
        super.onStop()
        mBottomSheetDialog?.let {
            mBottomSheetDialog?.dismiss()
        }
        viewModel.closeStream()
        binding.surfaceViewRendererLocal.release()
        binding.surfaceViewRendererRemote.release()
        rootEglBase?.release()
        rootEglBase = null
        mCameraManager = null
    }





    override fun observeDataChanges() {
        observe(viewModel.successData){
            when(it.action){
                SocketActionType.TERMINATE_CALL.type->{
                    findNavController().popBackStack()
                }
                SocketActionType.IM_ONLINE.type -> {
                  //  Toast.makeText(context,getString(R.string.customer_service_online), Toast.LENGTH_LONG).show()
                }
                SocketActionType.IM_OFFLINE.type -> {
                    findNavController().popBackStack()
                   // Toast.makeText(context,getString(R.string.customer_service_offline), Toast.LENGTH_LONG).show()
                }
                SocketActionType.END_CALL.type->{
                    findNavController().popBackStack()
                }
                SocketActionType.SUBREJECTED.type->{
                    findNavController().popBackStack()
                }
                SocketActionType.SDP.type->{
                    viewModel.handleSdpMessage(it)
                }
                SdpType.ANSWER.type->{
                    binding.relLayCallWaiting.visibility = View.GONE
                    binding.surfaceViewRendererRemote.visibility = View.VISIBLE
                    binding.surfaceViewRendererLocal.visibility = View.VISIBLE
                }
                SocketActionType.CANDIDATE.type->{
                    viewModel.handleSdpMessage(it)
                }
                SocketActionType.REQUEST_TAN.type->{
                    viewModel.tanId = it.tId.toString()
                    showBottomSheetDialog()
                }
                SocketActionType.TOGGLECAMERA.type->{
                    viewModel.switchCamera()
                }
                SocketActionType.TOGGLEFlASH.type->{
                    switchFlash()
                }
            }
        }

        observe(viewModel.errorData) {
            when(it){
                is ApiError ->{
                    it.message?.get(0)?.let { it1 -> Toasty.error(context!!, it1,Toast.LENGTH_SHORT).show() }
                }
                else -> {
                    Toasty.error(context!!,getString(R.string.connection_error_when_calling),Toast.LENGTH_LONG).show()
                    findNavController().popBackStack()
                    viewModel.errorData.removeObservers(this)
                }
            }

        }
        observe(viewModel.stateData){
            when(it){
                is State.Loading->{
                    mBottomSheetDialog?.let {
                        bottomBinding?.progressCircular?.visibility = View.VISIBLE
                        bottomBinding?.relLaySendSmsCode?.isEnabled = false
                    }
                }
                is State.Loaded->{
                    mBottomSheetDialog?.let {
                        bottomBinding?.progressCircular?.visibility = View.GONE
                        bottomBinding?.relLaySendSmsCode?.isEnabled = true
                    }
                }
            }
        }

        observe(viewModel.sharedValues.socketStatus){
            when(it){
                SocketConnectionStatus.CLOSE.type->{
                    Toasty.error(context!!,getString(R.string.connection_error_when_calling),Toast.LENGTH_LONG).show()
                    findNavController().popBackStack()
                    viewModel.sharedValues.socketStatus.removeObservers(this)
                }
            }
        }

        observe(viewModel.tanResponse){
            mBottomSheetDialog?.let {
                it.dismiss()
            }
        }


    }

    override fun onResume() {
        super.onResume()
        viewModel.startRtcProccessing()
    }

    private fun showBottomSheetDialog() {
         bottomBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_bottom_tan, null, false)
         mBehavior  =  BottomSheetBehavior.from(binding.bottomSheetContainer)
        if (mBehavior?.state === BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        }
         mBottomSheetDialog  = BottomSheetDialog(context!!, R.style.AppBottomSheetDialogTheme)
        bottomBinding?.root?.let { mBottomSheetDialog?.setContentView(it) }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
        mBottomSheetDialog?.show()
        bottomBinding?.relLaySendSmsCode?.setOnClickListener {
            viewModel.tanCode = bottomBinding?.etPin?.text.toString()
            viewModel.setSmsCode()
        }
        mBottomSheetDialog?.setOnDismissListener(DialogInterface.OnDismissListener {
            mBehavior = null
            mBottomSheetDialog = null
            bottomBinding = null
        })
    }


    fun switchFlash() {
        /*try {
            if (viewModel.currentCamera == CAMERA_BACK
                && isThereAFlash() != null
                && viewModel.sharedValues.camIsReady.value!!) {
                if (isThereAFlash()!!){
                    if (!viewModel.isTorchOn) {
                     *//* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                          flashlightProvider?.turnOnFlashLight() }
                        }else{*//*
                        try {
                            val cameraId = mCameraManager?.cameraIdList?.get(0)
                            cameraId?.let { if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCameraManager?.setTorchMode(it, true)
                            } else {
                                TODO("VERSION.SDK_INT < M")
                            }
                            } //Turn ON
                        } catch (e: CameraAccessException) {
                            e.printStackTrace()
                        }
                            viewModel.isTorchOn = true
                            viewModel.switchFlash(true)

                     //   }
                } else {
                    *//* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                         flashlightProvider?.turnOffFlashLight()
                     }

                        else{*//*
                        try {
                            val cameraId = mCameraManager?.cameraIdList?.get(0)
                            cameraId?.let { if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCameraManager?.setTorchMode(it, false)
                            } else {
                                TODO("VERSION.SDK_INT < M")
                            }
                            } //Turn ON
                        } catch (e: CameraAccessException) {
                            e.printStackTrace()
                        }
                        viewModel.isTorchOn = false
                        viewModel.switchFlash(true)
                     }


                    }
                }

        }catch (e: Exception){
            viewModel.switchFlash(false)
        }
*/
    }



    fun isThereAFlash()  =  context?.packageManager?.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)




    companion object{
        val CAMERA_FRONT = "1"
        val CAMERA_BACK = "0"
    }


    override fun getLayoutId(): Int = R.layout.fragment_started_call

    //endregion
}