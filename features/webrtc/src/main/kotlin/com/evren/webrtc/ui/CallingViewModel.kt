package com.evren.webrtc.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.evren.core.base.viewmodel.BaseViewModel
import com.evren.repository.interactors.SocketConnectInteractor
import com.evren.repository.interactors.base.None
import com.evren.repository.interactors.base.handle
import com.evren.repository.interactors.base.onFailure
import com.evren.repository.model.SocketActionType
import com.evren.repository.model.enum.RtcMessageType
import com.evren.repository.model.enum.SdpType
import com.evren.repository.model.socket.CallRejected
import com.evren.repository.model.socket.SocketResponse
import com.evren.repository.model.socket.TanResponse
import com.evren.repository.shared.SharedValues
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@ExperimentalCoroutinesApi
class CallingViewModel
@Inject constructor(
    private val socketConnection : SocketConnectInteractor,
    val sharedValues: SharedValues
) : BaseViewModel<SocketResponse>() {





    override suspend fun loadData() {
    }

    fun connectSocket() {
        viewModelScope.launch {
            sharedValues.customerInformation.value?.customerUid?.let {
                socketConnection(None()).collect {
                    it.handle(::handleState,::handleFailure,::handleSuccess)
                }
            }
        }
    }


    fun finishCall(){
        sharedValues.customerInformation.value?.customerUid?.let { room ->
                    viewModelScope.launch {
                        socketConnection.sendFinishCall(CallRejected(room = room))
            }

        }

    }




}