package com.evren.repository

internal const val DEFAULT_NAME = "Default name"
internal const val EMPTY_STRING = ""
const val SOCKET_BASE_URL ="wss://ws.identify24.de:8887/"
const val BASE_URL = "https://api.identify24.de/"
