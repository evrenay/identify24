package com.evren.repository.sources

import android.content.Context
import com.evren.repository.PersistenceRepository
import com.evren.repository.interactors.base.Failure
import com.evren.repository.interactors.base.PersistenceEmpty
import com.evren.repository.interactors.base.Result
import com.evren.repository.interactors.base.Success
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.persistence.IdentifyDatabase
import javax.inject.Inject

/**
 * Persistance source using Room database to save / read objects for SST - offline usage
 */
internal class PersistenceSource @Inject constructor(
    ctx: Context
) : PersistenceRepository() {

    //region Functions

    private val identifyDatabase = IdentifyDatabase.getInstance(ctx)


       override suspend fun getCustomerInfo(): Result<CustomerInformationEntity> =
        identifyDatabase
            .customerDao
            .getCustomerInformation()
            .takeIf { it != null }
            ?.run {
                Success(this)
            } ?: Failure(PersistenceEmpty())

    internal suspend fun saveCustomerInformation(customerInformationEntity: CustomerInformationEntity) {
        identifyDatabase.customerDao.saveCustomerInformation(customerInformationEntity)
    }

    //endregion
}
