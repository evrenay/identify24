package com.evren.repository.shared

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.model.enum.NavGraph
import com.evren.repository.model.enum.SocketConnectionStatus
import com.evren.repository.model.socket.SocketResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedValues @Inject constructor() {
    var inComingCall = MutableLiveData<SocketResponse?>()
    var customerInformation = MutableLiveData<CustomerInformationEntity?>()
    var socketStatus = MutableLiveData<String>()
    var changeNavGraph = MutableLiveData<String?>()
    var changeLanguage = MutableLiveData<String?>()
}