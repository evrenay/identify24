package com.evren.repository.model.enum

enum class SocketConnectionStatus(val type : String) {
    OPEN("O"),CLOSE("C"),EXCEPTION("E"),UNIDENTIFIED("U")
}
