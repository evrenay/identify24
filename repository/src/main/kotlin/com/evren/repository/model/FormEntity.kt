package com.evren.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.evren.repository.DEFAULT_NAME
import com.squareup.moshi.JsonClass

@Entity(tableName = "Form")
@JsonClass(generateAdapter = true)
data class FormEntity(
    @PrimaryKey val id: Long = 0L,
    val name: String = DEFAULT_NAME
)