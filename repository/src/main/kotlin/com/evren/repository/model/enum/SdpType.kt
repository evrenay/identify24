package com.evren.repository.model.enum

enum class SdpType(val type : String) {
    OFFER("offer"),ANSWER("answer"),CANDIDATE("candidate"),READY("ready")
}