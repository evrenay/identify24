package com.evren.repository.model.mrz

enum class DocType {
    PASSPORT, ID_CARD, OTHER
}