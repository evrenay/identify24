package com.evren.repository.model.socket

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CameraToggle(
    val action: String = "toggleCamera",
    val result: Boolean
)