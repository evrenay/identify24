package com.evren.repository.model.enum

enum class RtcMessageType(val type : String) {
    SDP("sdp"),CANDIDATE("candidate")
}