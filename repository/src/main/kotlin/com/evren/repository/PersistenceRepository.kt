package com.evren.repository

import com.evren.repository.interactors.base.Result
import com.evren.repository.model.CustomerInformationEntity

abstract class PersistenceRepository {
    internal abstract suspend fun getCustomerInfo() : Result<CustomerInformationEntity>
}