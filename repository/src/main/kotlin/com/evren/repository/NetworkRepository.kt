package com.evren.repository

import com.evren.repository.interactors.base.Result
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.model.dto.MrzDto
import com.evren.repository.model.dto.TanDto
import com.evren.repository.model.entities.TanEntity

/**
 * Contract for sources to seperate low level business logic from build and return type
 */
abstract class NetworkRepository {

    //region Abstractions


    internal abstract suspend fun getCustomerInformation(id : String) : Result<CustomerInformationEntity>
    internal abstract suspend fun setSmsCode(tanDto: TanDto) : Result<TanEntity?>
    internal abstract suspend fun setMrzData(mrzDto: MrzDto) : Result<CustomerInformationEntity?>
    //endregion
}
