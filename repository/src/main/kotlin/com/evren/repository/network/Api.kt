package com.evren.repository.network

import com.evren.repository.model.BaseApiResponse
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.model.dto.MrzDto
import com.evren.repository.model.dto.TanDto
import com.evren.repository.model.entities.TanEntity
import retrofit2.Response
import retrofit2.http.*

/**
 * Retrofit interface for networking
 */
internal interface Api {

    //region Get


    @Headers("Content-Type: application/json")
    @GET("mobile/getIdentDetails/{id}")
    suspend fun getCustomerInformation(
        @Path("id") id: String
    ): Response<BaseApiResponse<CustomerInformationEntity>>

    @Headers("Content-Type: application/json")
    @POST("mobile/nfc_verify")
    suspend fun setMrzData(
        @Body mrzDto: MrzDto
    ): Response<BaseApiResponse<CustomerInformationEntity?>>

    @Headers("Content-Type: application/json")
    @POST("mobile/verifyTan")
    suspend fun setSmsCode(
        @Body tanDto: TanDto
    ): Response<BaseApiResponse<TanEntity?>>
    //endregion
}
