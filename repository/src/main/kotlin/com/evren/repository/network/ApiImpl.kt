package com.evren.repository.network

import com.evren.repository.BASE_URL
import com.evren.repository.model.BaseApiResponse
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.model.dto.MrzDto
import com.evren.repository.model.dto.TanDto
import com.evren.repository.model.entities.TanEntity
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

internal const val TIMEOUT_DURATION = 7L

internal class ApiImpl @Inject constructor() : Api {

    //region Properties

    private val service by lazy {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        Retrofit.Builder()
            .client(
                OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_DURATION, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_DURATION, TimeUnit.SECONDS)
                    .addInterceptor {chain->
                        val newBuilder = chain.request().newBuilder()
                        chain.proceed(newBuilder.build())
                    }
                    .addInterceptor(
                        HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        }
                    ).build()
            )
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(BASE_URL)
            .build()
            .create(Api::class.java)
    }
    //endregion

    //region Functions


    override suspend fun getCustomerInformation(
        id: String
    ): Response<BaseApiResponse<CustomerInformationEntity>> =
        service.getCustomerInformation(id)

    override suspend fun setMrzData(mrzDto: MrzDto): Response<BaseApiResponse<CustomerInformationEntity?>>  =
        service.setMrzData(mrzDto)

    override suspend fun setSmsCode(tanDto: TanDto): Response<BaseApiResponse<TanEntity?>> =
        service.setSmsCode(tanDto)
    //endregion
}
