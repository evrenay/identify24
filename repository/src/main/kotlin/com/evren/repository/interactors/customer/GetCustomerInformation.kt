package com.evren.repository.interactors.customer

import com.evren.repository.interactors.base.*
import com.evren.repository.model.CustomerIdDto
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.socket.SocketSource
import com.evren.repository.sources.NetworkSource
import com.evren.repository.sources.PersistenceSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

@UseExperimental(ExperimentalCoroutinesApi::class)
class GetCustomerInformation @Inject constructor() : BaseInteractor<CustomerInformationEntity, GetCustomerInformation.Params>() {

    //region Properties

    @field:Inject
    internal lateinit var networkSource: NetworkSource

    @field:Inject
    internal lateinit var persistenceSource: PersistenceSource

    @field:Inject
    internal lateinit var socketSource: SocketSource



    //endregion

    //region Functions

    override suspend fun FlowCollector<Result<CustomerInformationEntity>>.run(params: Params) {
        val result = networkSource.getCustomerInformation(params.customerIdDto.identId)
        when(result){
            is Success -> {
               persistenceSource.saveCustomerInformation(result.successData)
                when(val resDB = persistenceSource.getCustomerInfo()){
                    is Success ->{

                    }
                    is Failure ->{
                        emit(resDB)
                    }
                }
            }
            is Failure ->{
                emit(result)
            }
        }
        emit(result)

    }
    //endregion

    data class Params(
        val customerIdDto: CustomerIdDto
    ) : InteractorParameters
}