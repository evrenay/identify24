package com.evren.repository.interactors.tan

import com.evren.repository.interactors.base.*
import com.evren.repository.model.dto.TanDto
import com.evren.repository.model.entities.TanEntity
import com.evren.repository.sources.NetworkSource
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject


class SetSmsCode @Inject constructor() : BaseInteractor<TanEntity?, SetSmsCode.Params>() {

    //region Properties

    @field:Inject
    internal lateinit var networkSource: NetworkSource

    //endregion

    //region Functions

    override suspend fun FlowCollector<Result<TanEntity?>>.run(params: Params) {
        val result = networkSource.setSmsCode(params.tanDto)
        emit(result)
    }


    data class Params(
        val tanDto: TanDto
    ) : InteractorParameters
    //endregion

}