package com.evren.repository.interactors.customer

import com.evren.repository.interactors.base.*
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.shared.SharedValues
import com.evren.repository.sources.PersistenceSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

@UseExperimental(ExperimentalCoroutinesApi::class)
class GetCustomerInfoFromDb @Inject constructor() : BaseInteractor<CustomerInformationEntity, None>() {

    //region Properties

    @field:Inject
    internal lateinit var sharedValues: SharedValues

    @field:Inject
    internal lateinit var persistenceSource: PersistenceSource

    //endregion

    //region Functions

    override suspend fun FlowCollector<Result<CustomerInformationEntity>>.run(params: None) {
        val result = persistenceSource.getCustomerInfo()
        when(result){
            is Success -> {
                sharedValues.customerInformation.postValue(result.successData)
                emit(result)
            }
            is Failure ->{
                emit(result)
            }
        }

    }
    //endregion

}