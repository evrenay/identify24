package com.evren.repository.interactors.mrz

import com.evren.repository.interactors.base.BaseInteractor
import com.evren.repository.interactors.base.InteractorParameters
import com.evren.repository.interactors.base.Result
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.model.dto.MrzDto
import com.evren.repository.sources.NetworkSource
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class SetMrzData @Inject constructor() : BaseInteractor<CustomerInformationEntity?, SetMrzData.Params>() {

    //region Properties

    @field:Inject
    internal lateinit var networkSource: NetworkSource

    //endregion

    //region Functions

    override suspend fun FlowCollector<Result<CustomerInformationEntity?>>.run(params: Params) {
        val result = networkSource.setMrzData(params.mrzDto)
        emit(result)
    }


    data class Params(
        val mrzDto: MrzDto
    ) : InteractorParameters
    //endregion

}