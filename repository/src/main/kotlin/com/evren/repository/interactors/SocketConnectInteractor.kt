package com.evren.repository.interactors

import com.evren.repository.interactors.base.*
import com.evren.repository.model.SocketActionType
import com.evren.repository.model.enum.SdpType
import com.evren.repository.model.enum.SocketConnectionStatus
import com.evren.repository.model.socket.*
import com.evren.repository.shared.SharedValues
import com.evren.repository.socket.RtcConnectionSource
import com.evren.repository.socket.SocketSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import kotlin.math.tan


@ExperimentalCoroutinesApi
class SocketConnectInteractor @Inject constructor() : BaseInteractor<SocketResponse, None>() {



    //region Properties

    @field:Inject
    internal lateinit var socketSource: SocketSource

    @field:Inject
    internal lateinit var sharedValues: SharedValues

    @field:Inject
    lateinit var rtcConnectionSource: RtcConnectionSource



    //endregion

    //region Functions


    override suspend fun FlowCollector<Result<SocketResponse>>.run(params: None) {

        socketSource.ifExistSocketConnection()
        socketSource.socketEvent().collect {

                when (it) {
                    is Success ->{
                        when(it.successData.action){
                            SocketConnectionStatus.OPEN.type -> {
                                val resp = sendNewSubscribe()
                                emit(resp)
                            }
                            SocketActionType.NEW_SUBSCRIBE.type->{
                                val resp = sendImOnline()
                                emit(resp)
                            }
                            SocketActionType.IM_ONLINE.type -> {
                                emit(it)
                            }
                            SocketActionType.IM_OFFLINE.type -> {
                                emit(it)
                            }
                            SocketActionType.INIT_CALL.type->{
                                emit(it)
                            }
                            SocketActionType.TERMINATE_CALL.type->{
                                emit(it)
                            }
                            SocketActionType.END_CALL.type->{
                                emit(it)
                            }
                            SocketActionType.SUBREJECTED.type->{
                                emit(it)
                            }
                            SocketActionType.REQUEST_TAN.type->{
                                emit(it)
                            }
                            SocketActionType.TOGGLECAMERA.type->{
                                emit(it)
                            }
                            SocketActionType.TOGGLEFlASH.type->{
                                emit(it)
                            }
                            SocketActionType.SDP.type ->{
                                val res = rtcConnectionSource.handleSdpMessage(socketResponse = it.successData)
                                emit(Success(SocketResponse(action = res)))
                            }
                            SocketActionType.CANDIDATE.type ->{
                                val res = rtcConnectionSource.handleSdpMessage(socketResponse = it.successData)
                                emit(Success(SocketResponse(action = res)))
                            }
                        }
                    }
                    is Failure ->{
                        emit(it)
                    }


                }
            }

    }




    fun closeSocketStream(){
        socketSource.webSocket?.let {
           it.close(4001,"")
        }

    }




        suspend fun startRtcProcess() : String{
            val res = rtcConnectionSource.handleSdpMessage(SocketResponse(SocketActionType.SDP.type,sdp = Sdp(
                type = SdpType.READY.type,sdp = null)))
            return res
        }



      fun sendNewSubscribe() : Result<SocketResponse>{
        sharedValues.customerInformation.value?.customerUid?.let { id ->
            return socketSource.sendSubscribe(SocketSubscribe(room = id))!!
        }
          return Failure(AuthenticationError())
    }

    fun sendImOnline(): Result<SocketResponse>{
        sharedValues.customerInformation.value?.customerUid?.let { id ->
            return socketSource.sendImOnline(ImOnline(room = id))!!
        }
        return Failure(AuthenticationError())
    }

    suspend fun sendStartCall(id : String) : Result<SocketResponse>{

        return when(val res = socketSource.startCall(StartCall(room = id))!!){
            is Success->{
                startRtcProcess()
                res
            }
            else -> {
                res
            }
        }
        }

    suspend fun sendTanResponse(tanResponse: TanResponse) : Result<SocketResponse>? = socketSource.sendTanResponse(tanResponse)

    suspend fun sendFinishCall(callRejected: CallRejected) : Result<SocketResponse>? = socketSource.sendFinishCall(callRejected)

    suspend fun sendFlashToggle(flashToggle: FlashToggle) : Result<SocketResponse>? = socketSource.sendFlashChanged(flashToggle)








}