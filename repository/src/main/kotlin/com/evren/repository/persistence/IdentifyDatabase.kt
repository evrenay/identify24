package com.evren.repository.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.evren.repository.model.CustomerInformationEntity
import com.evren.repository.persistence.dao.CustomerDao

const val DB_NAME = "IdentifyDB"

/**
 * DB that manages launches
 */
@Database(
    entities = [CustomerInformationEntity::class],
    exportSchema = true,
    version = 1
)
internal abstract class IdentifyDatabase : RoomDatabase() {

    //region Companion

    companion object {

        private lateinit var instance: IdentifyDatabase

        fun getInstance(ctx: Context): IdentifyDatabase {
            if (!Companion::instance.isInitialized) {
                instance = Room.databaseBuilder(ctx, IdentifyDatabase::class.java,
                    DB_NAME
                )
                    .build()
            }

            return instance
        }

    }
    //endregion

    //region Abstractions

    internal abstract val customerDao: CustomerDao
    //endregion
}
