package com.evren.repository.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.evren.repository.model.CustomerInformationEntity


@Dao
internal abstract class CustomerDao {

    //region Queries

    @Query("SELECT * FROM CustomerInformation")
    abstract suspend fun getCustomerInformation(): CustomerInformationEntity?

    //endregion

    //region Insertion

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun saveCustomerInformation(customerInformationEntity: CustomerInformationEntity)
    //endregion
}